import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import App from './App.vue'
import { routes } from './routes'
import store from './store/store'

const router = new VueRouter({
	mode: 'history',
	routes
});
Vue.use(VueRouter)
Vue.use(VueResource)

Vue.http.options.root = 'https://vuejs-stocks-trades.firebaseio.com/';

Vue.filter('currency',(value)=> {
return '$' + value.toLocaleString();
});
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
